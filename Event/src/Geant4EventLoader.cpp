#include <TFile.h>
#include <TTree.h>

#include "Geant4EventLoader.hpp"
#include "Utility.hpp"

Geant4EventLoader::Geant4EventLoader(std::string const& filePath)
    : mCurrentIndex(0)
{
    mInFile = TFile::Open(filePath.c_str());

    auto parametersTree =
            dynamic_cast<TTree*>(mInFile->Get(gGarametersTreeName));
    if(parametersTree == nullptr)
    {
        throw std::invalid_argument("Geant4EventLoader: Did not find "
                                    "parameters tree in input file.");
    }

    parametersTree->SetBranchAddress(gPhantomThicknessBranchName,
                                     &mParameters.mPhantomThickness);
    parametersTree->SetBranchAddress(gDetectorPhantomClearanceBranchName,
                                     &mParameters.mDetectorPhantomClearance);
    parametersTree->SetBranchAddress(gDetectorDetectorDistanceBranchName,
                                     &mParameters.mDetectorDetectorDistance);
    parametersTree->SetBranchAddress(gDetectorMaterialBudgetBranchName,
                                     &mParameters.mDetectorMaterialBudget);
    parametersTree->SetBranchAddress(gSourceDistanceBranchName,
                                     &mParameters.mSourceDistance);
    parametersTree->SetBranchAddress(gBeamEnergyBranchName,
                                     &mParameters.mBeamEnergy);
    parametersTree->SetBranchAddress(gBeamSizeBranchName,
                                     &mParameters.mBeamSize);
    int onlyEm;
    parametersTree->SetBranchAddress(gOnlyEmBranchName, &onlyEm);

    char simulationName[100]; // root really is from the past
    char worldMaterial[100];
    char phantomMaterial[100];
    char detectorMaterial[100];
    char beamParticle[100];

    parametersTree->SetBranchAddress(gSimulationNameBranchName,
                                     &simulationName);
    parametersTree->SetBranchAddress(gWorldMaterialBranchName, &worldMaterial);
    parametersTree->SetBranchAddress(gPhantomMaterialBranchName,
                                     &phantomMaterial);
    parametersTree->SetBranchAddress(gDetectorMaterialBranchName,
                                     &detectorMaterial);
    parametersTree->SetBranchAddress(gBeamParticleBranchName, &beamParticle);

    parametersTree->SetBranchAddress(gDetectorsFrontName,
                                     &mParameters.mDetectorsFront);
    parametersTree->SetBranchAddress(gDetectorsBackName,
                                     &mParameters.mDetectorsBack);
    parametersTree->SetBranchAddress(gNumberOfPrimariesBranchName,
                                     &mParameters.mNumberOfPrimaries);

    parametersTree->GetEntry(0);
    mParameters.mSimulationName = simulationName;
    mParameters.mWorldMaterial = worldMaterial;
    mParameters.mPhantomMaterial = phantomMaterial;
    mParameters.mDetectorMaterial = detectorMaterial;
    mParameters.mBeamParticle = beamParticle;
    mParameters.mOnlyEm = onlyEm == 1 ? true : false;

    auto xTree = dynamic_cast<TTree*>(mInFile->Get(gXTreeName));
    auto yTree = dynamic_cast<TTree*>(mInFile->Get(gYTreeName));
    auto zTree = dynamic_cast<TTree*>(mInFile->Get(gZTreeName));
    auto eTree = dynamic_cast<TTree*>(mInFile->Get(gETreeName));

    auto detXTree = dynamic_cast<TTree*>(mInFile->Get(gDetectorXTreeName));
    auto detYTree = dynamic_cast<TTree*>(mInFile->Get(gDetectorYTreeName));
    auto detZTree = dynamic_cast<TTree*>(mInFile->Get(gDetectorZTreeName));
    auto detEnergyTree =
            dynamic_cast<TTree*>(mInFile->Get(gDetectorEnergyTreeName));

    mTrees = {xTree,
              yTree,
              zTree,
              eTree,
              detXTree,
              detYTree,
              detZTree,
              detEnergyTree};

    for(auto tree : mTrees)
    {
        if(tree == nullptr)
        {
            throw std::invalid_argument(
                    "Geant4EventLoader: Improper TTree members in input file.");
        }
    }

    for(std::size_t i = 0; i < Event::gNumberOfLayers; i++)
    {
        xTree->SetBranchAddress((gXTreeName + std::to_string(i)).c_str(),
                                &mEvent.mPhantomPositions[i][0]);
        yTree->SetBranchAddress((gYTreeName + std::to_string(i)).c_str(),
                                &mEvent.mPhantomPositions[i][1]);
        zTree->SetBranchAddress((gZTreeName + std::to_string(i)).c_str(),
                                &mEvent.mPhantomPositions[i][2]);
        eTree->SetBranchAddress((gETreeName + std::to_string(i)).c_str(),
                                &mEvent.mPhantomEnergies[i]);
    }

    const auto lastFrontDetector = mParameters.mDetectorsFront > 0
                                           ? mParameters.mDetectorsFront - 1
                                           : 0;
    const auto firstBackDetector =
            mParameters.mDetectorsBack > 0 ? mParameters.mDetectorsFront : 0;
    detEnergyTree->SetBranchAddress(
            (gDetectorEnergyTreeName + std::to_string(lastFrontDetector))
                    .c_str(),
            &mEvent.mDetectorEnergyFront);
    detEnergyTree->SetBranchAddress(
            (gDetectorEnergyTreeName + std::to_string(firstBackDetector))
                    .c_str(),
            &mEvent.mDetectorEnergyBack);

    mEvent.mDetectorPositionsFront.resize(mParameters.mDetectorsFront);
    for(int i = 0; i < mParameters.mDetectorsFront; i++)
    {
        detXTree->SetBranchAddress(
                (gDetectorXTreeName + std::to_string(i)).c_str(),
                &mEvent.mDetectorPositionsFront[i][0]);
        detYTree->SetBranchAddress(
                (gDetectorYTreeName + std::to_string(i)).c_str(),
                &mEvent.mDetectorPositionsFront[i][1]);
        detZTree->SetBranchAddress(
                (gDetectorZTreeName + std::to_string(i)).c_str(),
                &mEvent.mDetectorPositionsFront[i][2]);
    }
    const auto i0 = mParameters.mDetectorsFront;
    mEvent.mDetectorPositionsBack.resize(mParameters.mDetectorsBack);
    for(int i = 0; i < mParameters.mDetectorsBack; i++)
    {
        detXTree->SetBranchAddress(
                (gDetectorXTreeName + std::to_string(i0 + i)).c_str(),
                &mEvent.mDetectorPositionsBack[i][0]);
        detYTree->SetBranchAddress(
                (gDetectorYTreeName + std::to_string(i0 + i)).c_str(),
                &mEvent.mDetectorPositionsBack[i][1]);
        detZTree->SetBranchAddress(
                (gDetectorZTreeName + std::to_string(i0 + i)).c_str(),
                &mEvent.mDetectorPositionsBack[i][2]);
    }
}

Geant4EventLoader::~Geant4EventLoader()
{
    mInFile->Close();
    delete mInFile;
    mInFile = nullptr;
}

bool Geant4EventLoader::LoadEvent(Event& outEvent)
{
    if(LoadSpecificEvent(outEvent, mCurrentIndex))
    {
        ++mCurrentIndex;
        return true;
    }
    return false;
}

bool Geant4EventLoader::LoadSpecificEvent(Event& outEvent,
                                          std::size_t eventNumber)
{
    if(NumberOfEvents() > eventNumber)
    {
        for(auto tree : mTrees)
        {
            tree->GetEntry(eventNumber);
        }
        outEvent = mEvent;
        return true;
    }
    return false;
}

std::size_t Geant4EventLoader::NumberOfEvents() const
{
    if(mTrees.size() > 0)
    {
        auto numberOfEvents = mTrees.front()->GetEntries();
        for(auto t = mTrees.begin() + 1; t != mTrees.end(); t++)
        {
            numberOfEvents = std::min(numberOfEvents, (*t)->GetEntries());
        }
        return static_cast<std::size_t>(numberOfEvents);
    }
    return 0;
}

Geant4EventLoader::Parameters const& Geant4EventLoader::GetParameters() const
{
    return mParameters;
}
