#include "DetectorErrors.hpp"

DetectorErrors::DetectorErrors(double uncertainty)
    : mErrorDistribution(
              uncertainty > 0 ? new std::pair<std::default_random_engine,
                                              std::normal_distribution<double>>(
                                        std::default_random_engine(),
                                        std::normal_distribution<double>(
                                                0, uncertainty))
                              : nullptr)
{
}

DetectorErrors::DetectorErrors(DetectorErrors const& other)
    : mErrorDistribution(nullptr)
{
    if(other.mErrorDistribution != nullptr)
    {
        mErrorDistribution = new std::pair<std::default_random_engine,
                                           std::normal_distribution<double>>(
                std::default_random_engine(),
                std::normal_distribution<double>(
                        0, other.mErrorDistribution->second.stddev()));
    }
}

DetectorErrors::~DetectorErrors()
{
    if(mErrorDistribution != nullptr)
    {
        delete mErrorDistribution;
        mErrorDistribution = nullptr;
    }
}

void DetectorErrors::operator()(Event& event) const
{
    if(mErrorDistribution != nullptr)
    {
        auto applyErrors = [&](std::vector<Eigen::Vector3d>& hits) {
            for(auto& p : hits)
            {
                p = {p.x() + (mErrorDistribution->second)(
                                     mErrorDistribution->first),
                     p.y() + (mErrorDistribution->second)(
                                     mErrorDistribution->first),
                     p.z()};
            }
        };

        applyErrors(event.mDetectorPositionsFront);
        applyErrors(event.mDetectorPositionsBack);
    }
}
