# Event

An event makes up the list-mode data that a proton computed tomography scanner
could produce with a pair of trackers and a calorimeter or time-of-flight
detector. Independent of the number of tracker modules (could be four to six),
the resulting event data is made up of ($`i \in [0, 99]`$):

- Positions $`\vec p_i = (x, y, z)_i`$,
- Directions, represented as slopes,
$`\vec k_i = (\mathrm dx/\mathrm d z, \mathrm dy/\mathrm d z)_i`$,
- Energies $`E_i`$ 

## Loader

An interface for loader classes `EventLoader` must be used to implement a
loader. It defines a single method `bool LoadEvent(Event_t&)` for loading a
single event. If this method returns `false`, the loader is treated as having no
further data. Otherwise, the parameter must be filled with the event data.

For simulation data, a concrete implementation `Geant4EventLoader` was made
available. 
