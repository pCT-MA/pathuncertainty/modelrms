#pragma once

#include <random>

#include "Event.hpp"

class TFile;
class TTree;

class Geant4EventLoader
{
public:
    struct Parameters
    {
    public:
        std::string mSimulationName;
        std::string mWorldMaterial;
        std::string mPhantomMaterial;
        std::string mDetectorMaterial;
        std::string mBeamParticle;
        float mPhantomThickness;
        float mDetectorPhantomClearance;
        float mDetectorDetectorDistance;
        float mDetectorMaterialBudget;
        float mSourceDistance;
        float mBeamEnergy;
        float mBeamSize;
        bool mOnlyEm;
        int mDetectorsFront;
        int mDetectorsBack;
        int mNumberOfPrimaries;
    };

    Geant4EventLoader(std::string const& filePath);
    ~Geant4EventLoader();

    /** @brief Sequentially loads subsequent events. */
    bool LoadEvent(Event& outEvent);
    /** @brief Load event with a given event number. */
    bool LoadSpecificEvent(Event& outEvent, std::size_t eventNumber);

    std::size_t NumberOfEvents() const;

    Parameters const& GetParameters() const;

private:
    static constexpr const char* gXTreeName = "x";
    static constexpr const char* gYTreeName = "y";
    static constexpr const char* gZTreeName = "z";
    static constexpr const char* gETreeName = "e";
    static constexpr const char* gGarametersTreeName = "parameters";

    static constexpr const char* gDetectorXTreeName = "detectorX";
    static constexpr const char* gDetectorYTreeName = "detectorY";
    static constexpr const char* gDetectorZTreeName = "detectorZ";
    static constexpr const char* gDetectorEnergyTreeName = "detectorE";

    static constexpr const char* gPhantomThicknessBranchName =
            "phantomThickness";
    static constexpr const char* gDetectorPhantomClearanceBranchName =
            "detectorPhantomClearance";
    static constexpr const char* gDetectorDetectorDistanceBranchName =
            "detectorDetectorDistance";
    static constexpr const char* gDetectorMaterialBudgetBranchName =
            "detectorMaterialBudget";
    static constexpr const char* gSourceDistanceBranchName = "sourceDistance";
    static constexpr const char* gBeamEnergyBranchName = "beamEnergy";
    static constexpr const char* gBeamSizeBranchName = "beamSize";
    static constexpr const char* gWorldMaterialBranchName = "worldMaterial";
    static constexpr const char* gPhantomMaterialBranchName = "phantomMaterial";
    static constexpr const char* gDetectorMaterialBranchName =
            "detectorMaterial";
    static constexpr const char* gBeamParticleBranchName = "beamParticle";
    static constexpr const char* gSimulationNameBranchName = "simulationName";
    static constexpr const char* gNumberOfPrimariesBranchName =
            "numberOfPrimaries";
    static constexpr const char* gDetectorsFrontName = "detectorsFront";
    static constexpr const char* gDetectorsBackName = "detectorsBack";
    static constexpr const char* gOnlyEmBranchName = "onlyEM";

    TFile* mInFile;
    std::array<TTree*, 8> mTrees;
    Event mEvent;
    std::size_t mCurrentIndex;
    Parameters mParameters;
};
