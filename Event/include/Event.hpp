#pragma once

#include <vector>

#include <Eigen/Geometry>

struct Event
{
    static constexpr std::size_t gNumberOfLayers = 100;

    double mDetectorEnergyFront;
    double mDetectorEnergyBack;
    /** @brief Hits obtained on detector planes. */
    std::vector<Eigen::Vector3d> mDetectorPositionsFront;
    std::vector<Eigen::Vector3d> mDetectorPositionsBack;

    /** @brief Ground truth of positions within phantom. */
    std::array<Eigen::Vector3d, gNumberOfLayers> mPhantomPositions;
    /** @brief Ground truth of energies within phantom. */
    std::array<double, gNumberOfLayers> mPhantomEnergies;
};
