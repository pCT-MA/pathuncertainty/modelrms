#pragma once

#include <random>

#include "Event.hpp"

class DetectorErrors
{
public:
    DetectorErrors(double uncertainty);
    DetectorErrors(DetectorErrors const& other);
    virtual ~DetectorErrors();
    void operator()(Event& event) const;

private:
    std::pair<std::default_random_engine, std::normal_distribution<double>>*
            mErrorDistribution;
};
