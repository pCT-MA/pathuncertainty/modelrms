#include "SingleKinkTrackModel.hpp"
#include "BoundaryConditions.hpp"

SingleKinkTrackModel::SingleKinkTrackModel(
        BoundaryConditions const* const event)
    : TrackModel(event)
{ /**/
}

Eigen::Vector3d
SingleKinkTrackModel::GetPointOfClosestApproach(double positionResolution) const
{
    Eigen::Vector3d h, g;
    std::tie(h, g) = GetClosestPoints(positionResolution);
    return 0.5 * (h + g);
}

std::pair<Eigen::Vector3d, Eigen::Vector3d>
SingleKinkTrackModel::GetClosestPoints(double positionResolution) const
{
    auto P0 = mBoundaryConditions->InPosition(positionResolution);
    auto P2 = mBoundaryConditions->OutPosition(positionResolution);
    const auto inSlope = mBoundaryConditions->InSlope(positionResolution);
    const auto outSlope = mBoundaryConditions->OutSlope(positionResolution);
    Eigen::Vector3d v{inSlope.x(), inSlope.y(), 1};
    Eigen::Vector3d w{outSlope.x(), outSlope.y(), 1};
    const auto n1 = v.cross(v.cross(w));
    const auto n2 = w.cross(v.cross(w));
    const auto h = (P0.dot(n1) - P2.dot(n1)) / w.dot(n1) * w + P2;
    const auto g = (P2.dot(n2) - P0.dot(n2)) / v.dot(n2) * v + P0;
    return {h, g};
}

double
SingleKinkTrackModel::GetSmallestDistance(double positionResolution) const
{
    Eigen::Vector3d h, g;
    std::tie(h, g) = GetClosestPoints(positionResolution);
    return (h - g).norm();
}

bool SingleKinkTrackModel::UseEvent(double positionResolution) const
{
    const auto poca = GetPointOfClosestApproach(positionResolution);
    auto P0 = mBoundaryConditions->InPosition(positionResolution);
    auto P2 = mBoundaryConditions->OutPosition(positionResolution);
    return P0.z() < poca.z() && poca.z() < P2.z();
}

Eigen::Vector2d SingleKinkTrackModel::ProjectXY(double z,
                                                double positionResolution) const
{
    auto P0 = mBoundaryConditions->InPosition(positionResolution);
    auto P2 = mBoundaryConditions->OutPosition(positionResolution);
    const auto poca = GetPointOfClosestApproach(positionResolution);

    const bool firstSegment = z < poca.z();
    const Eigen::Vector3d& start = firstSegment ? P0 : poca;
    const Eigen::Vector3d& end = firstSegment ? poca : P2;
    const auto difference = end - start;
    const auto t = (z - start.z()) / difference.z();
    const auto p = start + t * difference;
    return {p.x(), p.y()};
}
