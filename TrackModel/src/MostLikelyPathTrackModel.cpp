#include <numeric>

#include <Math/Integrator.h>
#include <TFile.h>
#include <TGraph.h>
#include <TTree.h>

#include "BoundaryConditions.hpp"
#include "Event.hpp"
#include "MostLikelyPathTrackModel.hpp"

std::vector<double> PolynomialWrapper::PolyFit(std::vector<double> const& X,
                                               std::vector<double> const& Y,
                                               std::size_t degree)
{
    std::vector<double> coefficients(degree);
    std::size_t size = std::min(X.size(), Y.size());
    TGraph graph(size, X.data(), Y.data());
    graph.LeastSquareFit(degree, coefficients.data());
    return coefficients;
}

double PolynomialWrapper::operator()(double z) const
{
    double result = 0;
    for(std::size_t i = 0; i < mCoefficients.size(); i++)
    {
        result += mCoefficients.at(i) * std::pow(z, i);
    }
    return result;
}

std::vector<double> const& PolynomialWrapper::GetCoefficients() const
{
    return mCoefficients;
}

void PolynomialWrapper::SetCoefficients(std::vector<double> const& coefficients)
{
    mCoefficients = coefficients;
}

double MomentumVelocity(double kineticEnergy, double restEnergy)
{
    return kineticEnergy * (kineticEnergy + 2 * restEnergy) /
           (kineticEnergy + 1 * restEnergy);
}

Geant4KinematicTerm::Geant4KinematicTerm(std::string const& inFileName,
                                         double restEnergy)
{
    TFile inFile(inFileName.c_str());
    auto zTree = dynamic_cast<TTree*>(inFile.Get(gZTreeName));
    auto eTree = dynamic_cast<TTree*>(inFile.Get(gETreeName));
    if(zTree == nullptr || eTree == nullptr)
    {
        throw std::invalid_argument(
                "Geant4KinematicTerm: Improper TTree members in input file.");
    }
    std::array<double, Event::gNumberOfLayers> Z;
    std::array<double, Event::gNumberOfLayers> E;

    for(std::size_t i = 0; i < Event::gNumberOfLayers; i++)
    {
        zTree->SetBranchAddress((gZTreeName + std::to_string(i)).c_str(),
                                &Z[i]);
        eTree->SetBranchAddress((gETreeName + std::to_string(i)).c_str(),
                                &E[i]);
    }

    const auto entries = std::min(zTree->GetEntries(), eTree->GetEntries());
    if(entries > 0)
    {
        std::vector<double> ZMax(Event::gNumberOfLayers);
        std::vector<double> PVInv(Event::gNumberOfLayers);
        for(std::size_t i = 0; i < entries; i++)
        {
            zTree->GetEntry(i);
            eTree->GetEntry(i);
            for(std::size_t j = 0; j < Event::gNumberOfLayers; j++)
            {
                ZMax[j] = std::max(ZMax.at(j), Z.at(j));
                PVInv[j] +=
                        1 / std::pow(MomentumVelocity(E.at(j), restEnergy), 2);
            }
        }

        for(std::size_t j = 0; j < Event::gNumberOfLayers; j++)
        {
            PVInv[j] /= static_cast<double>(entries);
        }

        SetCoefficients(PolyFit(ZMax, PVInv, 5));
    }

    inFile.Close();
}

MostLikelyPathTrackModel::MostLikelyPathTrackModel(
        BoundaryConditions const* const event,
        PolynomialWrapper* parameterisation,
        double radiationLength)
    : TrackModel(event), mParameterisation(std::move(parameterisation)),
      mRadiationLength(radiationLength)
{
}

MostLikelyPathTrackModel::~MostLikelyPathTrackModel()
{
    delete mParameterisation;
}

double EygesMoment(double a,
                   double b,
                   double i,
                   double X0,
                   PolynomialWrapper const& parameterisation)
{
    constexpr double e0 = 13.6; // E0 = 13.6 / speed_of_light  # MeV/c
    const auto prefix = e0 * (1 + 0.038 * std::log((b - a) / X0));

    auto functor = [&](double z) {
        return std::pow(b - z, i) / X0 * parameterisation(z);
    };
    ROOT::Math::Integrator integrator(functor);

    return std::pow(prefix, 2) * integrator.Integral(a, b);
}

bool MostLikelyPathTrackModel::UseEvent(double) const
{
    return mParameterisation != nullptr;
}

Eigen::Vector2d
MostLikelyPathTrackModel::ProjectXY(double z, double positionResolution) const
{
    using Matrix = Eigen::Matrix2d;

    const auto T0 = mBoundaryConditions->InSlope(positionResolution);
    const auto T2 = mBoundaryConditions->OutSlope(positionResolution);
    const auto P0 = mBoundaryConditions->InPosition(positionResolution);
    const auto P2 = mBoundaryConditions->OutPosition(positionResolution);

    const Eigen::Vector2d xIn(P0.x(), T0.x());
    const Eigen::Vector2d xOut(P2.x(), T2.x());
    const Eigen::Vector2d yIn(P0.y(), T0.y());
    const Eigen::Vector2d yOut(P2.y(), T2.y());
    const double uIn = P0.z();
    const double uOut = P2.z();

    const auto eIn1 =
            EygesMoment(uIn, z, 1, mRadiationLength, *mParameterisation);
    Matrix sigma1;
    sigma1 << EygesMoment(uIn, z, 2, mRadiationLength, *mParameterisation),
            eIn1, eIn1,
            EygesMoment(uIn, z, 0, mRadiationLength, *mParameterisation);

    const auto eOut1 =
            EygesMoment(z, uOut, 1, mRadiationLength, *mParameterisation);
    Matrix sigma2;
    sigma2 << EygesMoment(z, uOut, 2, mRadiationLength, *mParameterisation),
            eOut1, eOut1,
            EygesMoment(z, uOut, 0, mRadiationLength, *mParameterisation);
    Matrix r0;
    r0 << 1, z - uIn, 0, 1;
    Matrix r1;
    r1 << 1, uOut - z, 0, 1;

    const auto r1Inv = r1.inverse();
    const auto cIn = r1Inv * sigma2 *
                     (r1Inv * sigma2 + sigma1 * r1.transpose()).inverse() * r0;
    const auto cOut =
            sigma1 * (r1 * sigma1 + sigma2 * r1Inv.transpose()).inverse();

    auto xZ = cIn * xIn + cOut * xOut;
    auto yZ = cIn * yIn + cOut * yOut;
    return {xZ.x(), yZ.x()};
}
