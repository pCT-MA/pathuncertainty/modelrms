#include "SimpleSplineTrackModel.hpp"
#include "BoundaryConditions.hpp"

SimpleSplineTrackModel::SimpleSplineTrackModel(
        BoundaryConditions const* const event)
    : TrackModel(event)
{
}

bool SimpleSplineTrackModel::UseEvent(double) const
{
    return true;
}

Eigen::Vector2d
SimpleSplineTrackModel::ProjectXY(double z, double positionResolution) const
{
    const auto inPosition = mBoundaryConditions->InPosition(positionResolution);
    const auto outPosition =
            mBoundaryConditions->OutPosition(positionResolution);
    const auto inSlope = mBoundaryConditions->InSlope(positionResolution);
    const auto outSlope = mBoundaryConditions->OutSlope(positionResolution);
    const auto z0 = inPosition.z();
    const auto z2 = outPosition.z();
    Eigen::Matrix4d M;
    M << 1, z0, z0 * z0, z0 * z0 * z0, 1, z2, z2 * z2, z2 * z2 * z2, 0, 1,
            2 * z0, 3 * z0 * z0, 0, 1, 2 * z2, 3 * z2 * z2;
    auto MI = M.inverse();

    Eigen::Vector4d mAX = MI * Eigen::Vector4d(inPosition.x(),
                                               outPosition.x(),
                                               inSlope.x(),
                                               outSlope.x());
    Eigen::Vector4d mAY = MI * Eigen::Vector4d(inPosition.y(),
                                               outPosition.y(),
                                               inSlope.y(),
                                               outSlope.y());

    return {mAX.x() + mAX.y() * z + mAX.z() * z * z + mAX.w() * z * z * z,
            mAY.x() + mAY.y() * z + mAY.z() * z * z + mAY.w() * z * z * z};
}
