
#include "LinearTrackModel.hpp"
#include "BoundaryConditions.hpp"

LinearTrackModel::LinearTrackModel(
        BoundaryConditions const* const boundaryConditions)
    : TrackModel(boundaryConditions)
{ /**/
}

bool LinearTrackModel::UseEvent(double) const
{
    return true;
}

Eigen::Vector2d LinearTrackModel::ProjectXY(double z,
                                            double positionResolution) const
{
    auto P0 = mBoundaryConditions->InPosition(positionResolution);
    auto P2 = mBoundaryConditions->OutPosition(positionResolution);
    const auto dX = P2 - P0;
    const auto t = z - P0.z();
    return {P0.x() + t * dX.x() / dX.z(), P0.y() + t * dX.y() / dX.z()};
}
