#include "SingleKinkLinearFallbackTrackModel.hpp"
#include "LinearTrackModel.hpp"
#include "SingleKinkTrackModel.hpp"

SingleKinkLinearFallbackTrackModel::SingleKinkLinearFallbackTrackModel(
        BoundaryConditions const* const event)
    : mLinearModel(new LinearTrackModel(event)),
      mSingleKinkTrackModel(new SingleKinkTrackModel(event))
{
}

SingleKinkLinearFallbackTrackModel::~SingleKinkLinearFallbackTrackModel()
{
    delete mLinearModel;
    delete mSingleKinkTrackModel;
}

Eigen::Vector2d
SingleKinkLinearFallbackTrackModel::ProjectXY(double z,
                                              double positionResolution) const
{
    return mSingleKinkTrackModel->UseEvent(positionResolution)
                   ? mSingleKinkTrackModel->ProjectXY(z, positionResolution)
                   : mLinearModel->ProjectXY(z, positionResolution);
}

bool SingleKinkLinearFallbackTrackModel::UseEvent(
        double positionResolution) const
{
    return (mSingleKinkTrackModel->UseEvent(positionResolution)) ||
           (mLinearModel->UseEvent(positionResolution));
}
