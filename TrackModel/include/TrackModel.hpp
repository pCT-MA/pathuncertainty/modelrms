#pragma once

#include <Eigen/Geometry>

class BoundaryConditions;

/** @brief Interface for track models. */
class ITrackModel
{
public:
    virtual bool UseEvent(double positionResolution) const = 0;
    virtual Eigen::Vector2d ProjectXY(double z,
                                      double positionResolution) const = 0;

    virtual std::string GetName() const = 0;
    virtual std::string GetTitle() const = 0;
};

/** @brief Abstract base class of track models. */
class TrackModel : public ITrackModel
{
public:
    TrackModel(BoundaryConditions const* const boundaryConditions);

    const BoundaryConditions* const mBoundaryConditions;
};
