#pragma once

#include <Eigen/Geometry>

#include "TrackModel.hpp"

/** @brief Wraps a root polynomial */
class PolynomialWrapper
{
public:
    static std::vector<double> PolyFit(std::vector<double> const& X,
                                       std::vector<double> const& Y,
                                       std::size_t degree);

    double operator()(double z) const;
    std::vector<double> const& GetCoefficients() const;

    void SetCoefficients(std::vector<double> const& coefficients);

private:
    std::vector<double> mCoefficients;
};

class Geant4KinematicTerm : public PolynomialWrapper
{
public:
    /**
    @param restEnergy mass of a projectile, given in MeV/c².
    Defaults to proton mass. */
    Geant4KinematicTerm(std::string const& inFileName,
                        double restEnergy = 938.2720881604904);

private:
    static constexpr const char* gZTreeName = "z";
    static constexpr const char* gETreeName = "e";
};

class MostLikelyPathTrackModel : public TrackModel
{
public:
    /**
    @param radationLength given in mm. Defaults to water.
    */
    MostLikelyPathTrackModel(BoundaryConditions const* const boundaryConditions,
                             PolynomialWrapper* parameterisation,
                             double radiationLength = 361);
    virtual ~MostLikelyPathTrackModel();

    virtual bool UseEvent(double positionResolution) const override;
    virtual Eigen::Vector2d ProjectXY(double z,
                                      double positionResolution) const override;
    virtual std::string GetName() const override
    {
        return "MostLikelyPathTrackModel";
    }
    virtual std::string GetTitle() const override { return "Most likely path"; }

private:
    PolynomialWrapper* mParameterisation;
    double mRadiationLength;
};
