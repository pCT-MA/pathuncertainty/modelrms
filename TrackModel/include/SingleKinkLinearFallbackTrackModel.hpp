#pragma once

#include "TrackModel.hpp"

class LinearTrackModel;
class SingleKinkTrackModel;

class SingleKinkLinearFallbackTrackModel : public ITrackModel
{
public:
    SingleKinkLinearFallbackTrackModel(
            BoundaryConditions const* const boundaryConditions);
    ~SingleKinkLinearFallbackTrackModel();

    virtual bool UseEvent(double positionResolution) const override;
    virtual Eigen::Vector2d ProjectXY(double z,
                                      double positionResolution) const override;

    virtual std::string GetName() const override
    {
        return "SingleKinkLinearFallbackTrackModel";
    }
    virtual std::string GetTitle() const override
    {
        return "Kink, linear fallback";
    }

private:
    LinearTrackModel* mLinearModel;
    SingleKinkTrackModel* mSingleKinkTrackModel;
};
