#pragma once

#include <Eigen/Geometry>

#include "TrackModel.hpp"

class SimpleSplineTrackModel : public TrackModel
{
public:
    SimpleSplineTrackModel(BoundaryConditions const* const boundaryConditions);

    virtual bool UseEvent(double positionResolution) const override;
    virtual Eigen::Vector2d ProjectXY(double z,
                                      double positionResolution) const override;
    virtual std::string GetName() const override
    {
        return "SimpleSplineTrackModel";
    }
    virtual std::string GetTitle() const override { return "Simple spline"; }
};
