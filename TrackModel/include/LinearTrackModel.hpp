#pragma once

#include "TrackModel.hpp"

class LinearTrackModel : public TrackModel
{
public:
    LinearTrackModel(BoundaryConditions const* const boundaryConditions);

    virtual bool UseEvent(double positionResolution) const override;
    virtual Eigen::Vector2d ProjectXY(double z,
                                      double positionResolution) const override;
    virtual std::string GetName() const override { return "LinearTrackModel"; }
    virtual std::string GetTitle() const override { return "Linear"; }
};
