#pragma once

#include "TrackModel.hpp"

class SingleKinkTrackModel : public TrackModel
{
public:
    SingleKinkTrackModel(BoundaryConditions const* const boundaryConditions);

    Eigen::Vector3d GetPointOfClosestApproach(double positionResolution) const;

    std::pair<Eigen::Vector3d, Eigen::Vector3d>
    GetClosestPoints(double positionResolution) const;

    double GetSmallestDistance(double positionResolution) const;

    virtual bool UseEvent(double positionResolution) const override;
    virtual Eigen::Vector2d ProjectXY(double z,
                                      double positionResolution) const override;

    virtual std::string GetName() const override
    {
        return "SingleKinkTrackModel";
    }
    virtual std::string GetTitle() const override { return "Single kink"; }
};
