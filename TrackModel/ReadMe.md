# TrackModel

---
<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->

<!-- code_chunk_output -->

- [TrackModel](#trackmodel)
  - [Linear path](#linear-path)
  - [Single kink](#single-kink)
    - [Linear fallback model](#linear-fallback-model)
  - [Simple spline](#simple-spline)
  - [Most likely path](#most-likely-path)

<!-- /code_chunk_output -->
---

This class models the movement of a particle through a medium, given starting
and ending position and direction (slope). Given the z-coordinate of a
projection-plane, the model calculates the according positions in the x- and 
y-coordinates.

![TrackModel.png](doc/TrackModel.png)

Several track models are available, each of which calculates the projection
differently:

- _TrackModel_ - Abstract base class
    - LinearTrackModel - Linear interpolation between start- and endposition.
    This model is often used as a suboptimal reference in proton computed
    tomography papers.
    - SingleKinkTrackModel - Linear interpolation between start- and endposition
    using a single kink at the point of closest approach. 
    - SimpleSplineTrackModel - A cubic spline that matches the start and
    end positions and directions.
    - MostLikelyPathTrackModel - Uses the most likely path of a charged
    particle.

![](doc/TrackModels.png)

In the following math, position vectors $`\vec u_\text{in}`$ and
$`\vec u_\text{out}`$ are defined as ($`i \in [\text{in}, \text{out}]`$):

```math
\vec u_i = \begin{bmatrix}t_i \\ \theta_{i} \end{bmatrix},
```

where $`t_i`$ is the displacement perpendicular to the $`z`$-axis, e.g. one of
the two coordinates $`x_i`$ and $`y_i`$, and
$`\theta_i = \tan(\Delta t_i/\Delta z)`$ is the angle of the projected direction 
vector. Because the coordinates are arbitrary, both can be calculated completely 
separate and results are usually displayed only for the abstract coordinate
$`u`$. In the programs, these calculations are simply repeated for $`x`$ and
$`y`$.

## Linear path

The linear path ignores the angle information, and calculates a new direction
angle using only the positions $`t_i`$:

```math
\vec u(z) = \begin{bmatrix}
t_\text{in} + (t_\text{out} - t_\text{in}) \times \frac {(z - z_0)} {z_2 - z_0} \\ 
\tan\left(\frac{t_\text{out} - t_\text{in}}{z_2 - z_0}\right) \end{bmatrix}
```

## Single kink

The single kink model is made up of two linear track segments that make use of
the direction information given by the measurements. First, the two points
$`\vec h, \vec g`$ on each of the two skew lines closest to the other line are
identified. Using the measurements $`\vec p_i = (x, y, z)_i`$ and
$`\vec v_i = (\mathrm dx/\mathrm dz, \mathrm dy/\mathrm dz, 1)_i`$, where
$`i \in [\text{in}, \text{out}]`$:

The direction vectors are normalised. I am not quite sure, if this is a waste of
computing time though.

```math
\vec v_i = \vec v_i / \sqrt{|\vec v_i \cdot \vec v_i|}.
```

Then, normal vectors are constructed according to 

```math
\vec n_\text{in} =
\vec v_\text{in} \times \vec v_\text{in} \times \vec v_\text{out}
```

and

```math
\vec n_\text{out} =
\vec v_\text{out} \times \vec v_\text{in} \times \vec v_\text{out}.
```

Finally, the closest points are: 

```math
\vec h =
\frac{(\vec p_\text{in} \cdot \vec n_\text{in} -
        \vec p_\text{out} \cdot \vec n_\text{in})}
    {\vec v_\text{out} \cdot \vec n_\text{in}} \times
\vec v_\text{out} + \vec p_\text{out}
```

and 

```math
\vec g =
\frac{(\vec p_\text{out} \cdot \vec n_\text{out} -
        \vec p_\text{in} \cdot \vec n_\text{out})}
    {\vec v_\text{in} \cdot \vec n_\text{out}} \times
\vec v_\text{in} + \vec p_\text{in}.
```

The point of closest approach
$`\vec p_\text{CA} = 0.5 (\vec p_\text{in} + \vec p_\text{out})`$ is used to
reject tracks, when it's $`z_\text{CA}`$ is not found within a region of
interest between the inner detectors or the smallest distance
$`\sqrt{(\vec h - \vec g) \cdot (\vec h - \vec g)}`$ is too large. 

To find the track position for an arbitrary $`z`$, first, two end points
$`\vec a, \vec b`$ are selected based on $`z`$ and $`z_\text{CA}`$:

```math
\vec a = \left\{ \begin{matrix} p_\text{in}, & z < z_\text{CA} \\
p_\text{CA}, & z \geq z_\text{CA} \end{matrix} \right.
```

and

```math
\vec b = \left\{ \begin{matrix} p_\text{CA}, & z < z_\text{CA} \\
p_\text{out}, & z \geq z_\text{CA} \end{matrix} \right..
```

Finally, the projected coordinates are returned with

```math
\vec p(z) = \vec a + \frac{z - z_a}{z_b - z_a} (\vec b - \vec a).
```

### Linear fallback model

An alternative track model `SingleKinkLinearFallbackTrackModel` is available 
because the single kink model rejects a lot of the tracks that would be accepted
by both the spline or most likely path models. This fallback model is no
different from the single kink model mathematically. The only difference is that
the linear model is used when the single kink model rejects a track.

## Simple spline

A cubic spline can be used to interpolate between the entry and exit positions,
using angle information as well. This was first suggested by
[Li, 2006](https://doi.org/10.1118/1.2171507) as a function

```math
t(z) = a_0 + a_1 z + a_2 z^2 + a_3 z^3,
```

where the coefficients $`a_i`$ are matched to the boundary conditions. The
authors did not explain this matching process, but the calculation is quite
simple. The first derivative of $`t(z)`$ is: 

```math
\frac {\mathrm{d}t(z)} {\mathrm{d}z} = a_1 + 2 a_2 z + 3 a_3 z^2
```

Using the known boundary conditions, these become:

```math
\vec b_{u} = 
\begin{bmatrix} t_\text{in} \\ t_\text{out} \\ 
\theta_\text{in} \\ \theta_\text{out} \end{bmatrix} = 
\begin{bmatrix}
1 && z_\text{in}    && z_\text{in}^2    && z_\text{in}^3    \\
1 && z_\text{out}   && z_\text{out}^2   && z_\text{out}^3   \\
0 && 1              && 2 z_\text{in}    && 3 z_\text{in}^2  \\
0 && 1              && 2 z_\text{out}   && 3 z_\text{out}^2
\end{bmatrix}
\begin{bmatrix} a_0 \\ a_1 \\ a_2 \\ a_3 \end{bmatrix} = M \vec a_u,
```

or

```math
\vec a_u = M^{-1} \vec b_u.
```

It should be noted, that another spline model was suggested previously
[Collins-Fekete, 2015](https://doi.org/10.1088/0031-9155/60/13/5071), which
takes initial energy and energy loss into account.

## Most likely path

The most-likely path (MLP) is (current implementation) calculated using either
[Schulte, 2008](https://doi.org/10.1118/1.2986139), 
[Collins-Fekete, 2017](https://doi.org/10.1088/1361-6560/aa58ce) or
[Krah, 2018](https://doi.org/10.1088/1361-6560/aaca1f). In this project, the
last source was used as source.

Eyges moments define the scattering power at a depth $`z \in (a, b)`$ in a
material:

```math
\mathcal E_i(a, b|X_0) =
\left(\frac{13.6 \text{ MeV}}{c}\right)^2
\left[1 + 0.038 \ln \left(\frac {b - a}{X_0}\right)\right]^2
\int_a^b \frac{(b - z)^i}{\beta^2(z)p^2(z)} \frac {\mathrm d z}{X_0(z)},
```

where $`X_0`$ is the radiation length of the material. The kinematic term
$`1/(\beta cp)^2`$ is parameterised as a polynomial: 

```math
\frac{1}{(\beta c p)^2}(z) = \sum_{i = 0}^5 a_i z^i,
```

so that the integral can be calculated numerically. These moments define the
$`2 \times 2`$ scattering matrices for the in- ($`\Sigma_1`$) and outbound
($`\Sigma_2`$) track segments:

```math
\Sigma_1 =
\begin{bmatrix}\mathcal E_2(z_0, z_1|X_0) && \mathcal E_1(z_0, z_1|X_0) \\
\mathcal E_1(z_0, z_1|X_0) && \mathcal E_0(z_0, z_1|X_0) \end{bmatrix},
```

and

```math
\Sigma_2 =
\begin{bmatrix}\mathcal E_2(z_1, z_2|X_0) && \mathcal E_1(z_1, z_2|X_0) \\
\mathcal E_1(z_1, z_2|X_0) && \mathcal E_0(z_1, z_2|X_0) \end{bmatrix}.
```

Finally, using a small-angle approximation, the translation matrices for the in-
($`R_0`$) and outbound ($`R_1`$) track segments are given by:

```math
R_0 = \begin{bmatrix}1 && z - z_\text{in} \\ 0 && 1 \end{bmatrix},\ 
R_1 = \begin{bmatrix}1 && z_\text{out} - z \\ 0 && 1 \end{bmatrix}
```

Using the above, the most likely path $`\vec u(z)`$ becomes

```math
\vec u(z) = \begin{bmatrix}t(z) \\ \theta(z)\end{bmatrix} = 
C_\text{in} \vec u_\text{in} + C_\text{out} u_\text{out}, 
```

where

```math
C_\text{in} =
R_1^{-1} \Sigma_2 (R_1^{-1} \Sigma_2 + \Sigma_1 R_1^T)^{-1} R_0
```

and

```math
C_\text{out} = 
\Sigma_1(R_1 \Sigma_1 + \Sigma_2 (R_1^{-1})^T)^{-1}
```
