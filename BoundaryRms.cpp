#include <iostream>

#include <TFile.h>
#include <TTree.h>

#include "ProgressBar.hpp"

#include "BoundaryConditions.hpp"
#include "Cut.hpp"
#include "DetectorErrors.hpp"
#include "Geant4EventLoader.hpp"
#include "Utility.hpp"

void PrintMessage(std::string const& message);
void PrintProgress(unsigned i, unsigned I, unsigned mod = 25);

class Arguments
{
public:
    Arguments(int argc, char const* argv[]);

    std::string const& InFileName() const { return mInFileName; }
    std::string const& OutSuffix() const { return mOutSuffix; }
    std::vector<double> const& SpatialResolutions() const
    {
        return mSpatialResolutions;
    }
    bool DoFilter() const { return mDoFilter; }
    bool CircularFilter() const { return mCircFilter; }
    bool PlotGraph() const { return mGraph; }
    bool UseGbl() const { return mGbl; }

    bool Valid() const;

    std::string ToString() const;

private:
    std::string mInFileName;
    std::string mOutSuffix;
    std::vector<double> mSpatialResolutions;
    bool mDoFilter;
    bool mCircFilter;
    bool mGraph;
    bool mGbl;
};

double DifferenceSquared(Eigen::Vector2d const& a, Eigen::Vector2d const& b)
{
    auto diff = a - b;
    return (diff.x() * diff.x()) + (diff.y() * diff.y());
};

int main(int argc, const char* argv[])
{
    const Arguments arguments(argc, argv);
    PrintMessage(arguments.ToString());
    if(!arguments.Valid())
    {
        PrintMessage(
                "Use: ./BoundaryRms Data.root [--nofilter || -n] "
                "[--circFilter || -c] [--graph || -g] [SigmaP0, SigmaP1, …]"
                "[--suffix || -s suffix] [--gbl || -b]");
        std::exit(EXIT_FAILURE);
    }

    const auto outFileName =
            arguments.InFileName().substr(
                    0, arguments.InFileName().find_last_of(".")) +
            "_boundaryrms" + arguments.OutSuffix() + ".root";
    {
        TFile outFile(outFileName.c_str(), "RECREATE");
        outFile.Write();
        outFile.Close();
    }

    Geant4EventLoader loader(arguments.InFileName());
    std::vector<DetectorErrors> errors;
    for(auto sigmaP : arguments.SpatialResolutions())
    {
        errors.push_back(DetectorErrors(sigmaP));
    }
    Event event;
    Event errorEvent;
    std::unique_ptr<BoundaryConditions> boundaries;
    arguments.UseGbl()
            ? boundaries.reset(new GeneralBrokenLinesBoundaryConditions(
                      &errorEvent,
                      loader.GetParameters().mDetectorMaterialBudget,
                      loader.GetParameters().mSourceDistance))
            : boundaries.reset(new StraightLineBoundaryConditions(
                      &errorEvent, loader.GetParameters().mSourceDistance));

    /** @brief Maps
    [boundaryModel][sigmaP][front/back] → sum[(x-x_t)_i^2 + (y-y_t)_i^2]
    @todo Direction errors are not yet calculated */
    std::vector<std::array<double, 4>> quadSum{errors.size()};

    /** @todo These cuts internally use a boundary conditions
     * in the constructor! */
    EventCutSpatialResolution* eventCut;
    arguments.DoFilter()
            ? arguments.CircularFilter()
                      ? eventCut = new CircularEventCutGeant4(
                                arguments.InFileName(),
                                arguments.PlotGraph() ? outFileName : "",
                                arguments.SpatialResolutions())
                      : eventCut = new RectangularEventCutGeant4(
                                arguments.InFileName(),
                                arguments.PlotGraph() ? outFileName : "",
                                arguments.SpatialResolutions())
            : eventCut = new NullCutSpatialResolution;
    std::vector<std::size_t> cutEvents(arguments.SpatialResolutions().size(),
                                       0);
    const auto numberOfEvents = loader.NumberOfEvents();
    std::size_t iData = 0;
    const auto fivePercentEvents = static_cast<std::size_t>(
            static_cast<double>(numberOfEvents) * 0.05); // for output printing
    while(loader.LoadEvent(event))
    {
        ++iData;
        for(std::size_t iSigmaP = 0;
            iSigmaP < arguments.SpatialResolutions().size();
            iSigmaP++)
        {
            errorEvent = event;
            errors.at(iSigmaP)(errorEvent);
            const auto sigmaP = arguments.SpatialResolutions().at(iSigmaP);

            if((*eventCut)(*boundaries.get(),
                           arguments.SpatialResolutions().at(iSigmaP)))
            {
                ++cutEvents[iSigmaP];
                continue;
            }

            // first element
            {
                const auto inPosition = boundaries->InPosition(sigmaP);
                const auto frontPosition = errorEvent.mPhantomPositions.front();
                quadSum[iSigmaP][0] += DifferenceSquared(
                        {inPosition.x(), inPosition.y()},
                        {frontPosition.x(), frontPosition.y()});
            }

            // last element
            {
                const auto outPosition = boundaries->OutPosition(sigmaP);
                const auto backPosition = errorEvent.mPhantomPositions.back();
                quadSum[iSigmaP][1] +=
                        DifferenceSquared({outPosition.x(), outPosition.y()},
                                          {backPosition.x(), backPosition.y()});
            }
        }
        PrintProgress(iData,
                      numberOfEvents,
                      fivePercentEvents > 100 ? fivePercentEvents : 100);
    }

    auto loaderParameters = loader.GetParameters();
    PrintMessage("Total events read: " + std::to_string(iData) +
                 ", of which were cut:             ");
    for(std::size_t i = 0; i < arguments.SpatialResolutions().size(); i++)
    {
        PrintMessage("SigmaP: " +
                     std::to_string(arguments.SpatialResolutions().at(i)) +
                     " - " + std::to_string(cutEvents.at(i)) + " events");
    }
    TFile outFile(outFileName.c_str(), "UPDATE");
    TTree outTree("boundaryRms", "boundaryRms");
    Double_t sigmaP;
    Bool_t frontOrBack;
    Double_t rms;
    Int_t filtering =
            arguments.DoFilter() ? arguments.CircularFilter() ? 2 : 1 : 0;
    Int_t onlyEm = loaderParameters.mOnlyEm ? 1 : 0;
    outTree.Branch(
            "worldMaterial", "std::string", &loaderParameters.mWorldMaterial);
    outTree.Branch("phantomMaterial",
                   "std::string",
                   &loaderParameters.mPhantomMaterial);
    outTree.Branch("detectorMaterial",
                   "std::string",
                   &loaderParameters.mDetectorMaterial);
    outTree.Branch(
            "beamParticle", "std::string", &loaderParameters.mBeamParticle);
    outTree.Branch("phantomThickness", &loaderParameters.mPhantomThickness);
    outTree.Branch("detectorClearance",
                   &loaderParameters.mDetectorPhantomClearance);
    outTree.Branch("detectorDistance",
                   &loaderParameters.mDetectorDetectorDistance);
    outTree.Branch("materialBudget", &loaderParameters.mDetectorMaterialBudget);
    outTree.Branch("sourceDistance", &loaderParameters.mSourceDistance);
    outTree.Branch("beamEnergy", &loaderParameters.mBeamEnergy);
    outTree.Branch("numberOfPrimaries", &loaderParameters.mNumberOfPrimaries);
    outTree.Branch("sigmaP", &sigmaP);
    outTree.Branch("frontOrBack", &frontOrBack);
    outTree.Branch("rms", &rms);
    outTree.Branch("filtering", &filtering);
    outTree.Branch("onlyEM", &onlyEm);
    outTree.Branch("detectorsFront", &loaderParameters.mDetectorsFront);
    outTree.Branch("detectorsBack", &loaderParameters.mDetectorsBack);
    outTree.Branch("beamSize", &loaderParameters.mBeamSize);

    // for the z positions: just take the most recent event read
    std::vector<double> Z;
    for(std::size_t iZ = 0; iZ < Event::gNumberOfLayers; iZ++)
    {
        Z.emplace_back(event.mPhantomPositions.at(iZ).z());
    }

    for(std::size_t iSigmaP = 0;
        iSigmaP < arguments.SpatialResolutions().size();
        iSigmaP++)
    {
        sigmaP = arguments.SpatialResolutions().at(iSigmaP);

        frontOrBack = false;
        rms = std::sqrt(quadSum.at(iSigmaP).at(0) /
                        static_cast<double>(
                                2 * (numberOfEvents - cutEvents.at(iSigmaP))));
        outTree.Fill();
        frontOrBack = true;
        rms = std::sqrt(quadSum.at(iSigmaP).at(1) /
                        static_cast<double>(
                                2 * (numberOfEvents - cutEvents.at(iSigmaP))));
        outTree.Fill();
    }
    outTree.Write();
}

void PrintMessage(std::string const& message)
{
    std::cout << message << std::endl;
}

void PrintProgress(unsigned i, unsigned I, unsigned mod)
{
    if(i % mod == 0)
    {
        std::cout << ProgressBar(i,
                                 I,
                                 "Events accepted: ",
                                 " " + std::to_string(i) + "/" +
                                         std::to_string(I));

        if(i < I)
        {
            std::cout << " \r";
            std::cout.flush();
        }
        else
        {
            std::cout << std::endl;
        }
    }
}

Arguments::Arguments(int argc, char const* argv[])
    : mDoFilter(true), mCircFilter(false), mGraph(false), mGbl(false)
{
    for(int i = 1; i < argc; i++)
    {
        const std::string argument(argv[i]);
        if(argument == "-n" || argument == "--nofilter")
        {
            mDoFilter = false;
        }
        else if(argument == "-c" || argument == "--circFilter")
        {
            mCircFilter = true;
        }
        else if(argument == "-g" || argument == "--graph")
        {
            mGraph = true;
        }
        else if(argument == "-s" || argument == "--suffix")
        {
            if(i + 1 < argc)
            {
                mOutSuffix = argv[i + 1];
                ++i;
            }
        }
        else if(argument == "-b" || argument == "--gbl")
        {
            mGbl = true;
        }
        else
        {
            double number = 0;
            std::stringstream stream(argument);
            stream >> number;
            if(number > 0)
            {
                if(!Contains(mSpatialResolutions, number))
                {
                    mSpatialResolutions.push_back(number);
                    continue;
                }
            }
            mInFileName = argument;
        }
    }
    if(mSpatialResolutions.empty())
    {
        mSpatialResolutions.push_back(0);
    }
}

bool Arguments::Valid() const
{
    return mInFileName.size() > 0;
}

std::string Arguments::ToString() const
{
    std::string spatialResolutionsString = "";
    for(const auto& resolution : SpatialResolutions())
    {
        spatialResolutionsString += "\n\t" + std::to_string(resolution);
    }
    return "InFileName        : " + InFileName() +
           "\nOutSuffix         : " + OutSuffix() +
           "\nSpatialResolutions: " + spatialResolutionsString +
           "\nDo filter?          " + (DoFilter() ? "yes" : "no") +
           "\nUse GBL?            " + (UseGbl() ? "yes" : "no") +
           "\nCircular filter?    " + (CircularFilter() ? "yes" : "no") +
           "\nPlot debug graphs?  " + (PlotGraph() ? "yes" : "no") +
           "\nArgs valid?         " + (Valid() ? "yes" : "no");
}
