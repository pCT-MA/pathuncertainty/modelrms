#pragma once

#include <iomanip>
#include <sstream>
#include <vector>

class TObject;

void SetDefaultRootStyle();
void Render(TObject& object,
            std::string const& outFileName,
            std::string const& legendTitle,
            std::vector<std::string> const& annotations = {});

template <typename T>
std::string ToStringWithPrecision(T const& t, std::size_t precision = 2)
{
    std::stringstream stream;
    stream << std::fixed << std::setprecision(precision) << t;
    return stream.str();
}

template <typename T>
bool Contains(std::vector<T> const& collection, T const& t)
{
    for(const auto& value : collection)
    {
        if(value == t)
        {
            return true;
        }
    }
    return false;
}