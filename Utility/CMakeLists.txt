set(CMAKE_CXX_STANDARD 14)
project(Utility CXX)

# ROOT
# Use: target_link_libraries(${PROJECT_NAME} ${ROOT_LIBRARIES})
list(APPEND CMAKE_PREFIX_PATH $ENV{ROOTSYS})
find_package(ROOT REQUIRED COMPONENTS RIO)
include(${ROOT_USE_FILE})

add_library(${PROJECT_NAME}
        src/Utility.cpp)
target_include_directories(${PROJECT_NAME} PUBLIC include)
target_link_libraries(${PROJECT_NAME} ${ROOT_LIBRARIES})
