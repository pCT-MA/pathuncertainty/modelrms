#include <TCanvas.h>
#include <TColor.h>
#include <TLegend.h>
#include <TObject.h>
#include <TPaveText.h>
#include <TROOT.h>

#include "Utility.hpp"

void SetDefaultRootStyle()
{
    constexpr std::array<uint32_t, 8> paletteOkabeIto{0x0,
                                                      0xE69F00,
                                                      0x56B4E9,
                                                      0x009E73,
                                                      0xF0E442,
                                                      0x0072B2,
                                                      0xD55E00,
                                                      0xCC79A7};
    constexpr std::array<std::size_t, 9> customOrder{0, 0, 1, 2, 3, 7, 5, 6, 4};
    auto Channel = [](uint32_t value, std::size_t channel) {
        const auto shift = channel * 8u;
        return static_cast<float>((value & (0xFF << shift)) >> shift) / 255.f;
    };
    for(std::size_t i = 0; i < customOrder.size(); i++)
    {
        const auto value = paletteOkabeIto.at(customOrder.at(i));
        const auto r = Channel(value, 2);
        const auto g = Channel(value, 1);
        const auto b = Channel(value, 0);
        auto colour = gROOT->GetColor(1 + i);
        colour->SetRGB(r, g, b);
    }
}

void Render(TObject& object,
            std::string const& outFileName,
            std::string const& legendTitle,
            std::vector<std::string> const& annotations)
{
    TCanvas canvas("", "", 640, 480);
    canvas.cd(); // hideous
    canvas.SetMargin(0.1, 0.25, 0.1, 0.1);
    object.Draw("apl");

    auto legend =
            canvas.BuildLegend(0.775, 0.66, 0.975, 0.875, legendTitle.c_str());
    if(legend->GetNRows() > 6)
    {
        legend->SetNColumns(2);
    }
    legend->SetFillColorAlpha(0, 0);

    // root wouldn't render the annotation if it was allocated on the stack
    TPaveText* annotation = nullptr;
    if(annotations.size() > 0)
    {
        annotation = new TPaveText(0.75, 0.1, 0.95, 0.45, "NDC");
        annotation->SetTextAlign(kHAlignLeft | kVAlignTop);
        annotation->SetBorderSize(0);
        annotation->SetFillColorAlpha(0, 0);
        for(const auto& note : annotations)
        {
            annotation->AddText(note.c_str());
        }
        annotation->Draw();
    }

    canvas.SaveAs(outFileName.c_str());
    if(annotation != nullptr)
    {
        delete annotation;
    }
};
