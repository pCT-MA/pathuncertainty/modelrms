#pragma once

#include <Eigen/Geometry>

class Event;

class BoundaryConditions
{
public:
    BoundaryConditions(const Event* const event, double sourceZ);

    const Event* const mEvent;
    const double mSourceZ;

    virtual Eigen::Vector3d InPosition(double error) const = 0;
    virtual Eigen::Vector3d OutPosition(double error) const = 0;

    virtual Eigen::Vector2d InSlope(double error) const = 0;
    virtual Eigen::Vector2d OutSlope(double error) const = 0;
};

class StraightLineBoundaryConditions : public BoundaryConditions
{
public:
    StraightLineBoundaryConditions(const Event* const event, double sourceZ);

    virtual Eigen::Vector3d InPosition(double error) const override;
    virtual Eigen::Vector3d OutPosition(double error) const override;

    virtual Eigen::Vector2d InSlope(double error) const override;
    virtual Eigen::Vector2d OutSlope(double error) const override;
};

class GeneralBrokenLinesBoundaryConditions : public BoundaryConditions
{
public:
    GeneralBrokenLinesBoundaryConditions(const Event* const event,
                                         double materialBudget,
                                         double sourceZ);

    virtual Eigen::Vector3d InPosition(double error) const override;
    virtual Eigen::Vector3d OutPosition(double error) const override;

    virtual Eigen::Vector2d InSlope(double error) const override;
    virtual Eigen::Vector2d OutSlope(double error) const override;

private:
    double mMaterialBudget;
};
