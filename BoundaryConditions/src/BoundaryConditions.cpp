#include "GblPoint.h"
#include "GblTrajectory.h"

#include "BoundaryConditions.hpp"
#include "Event.hpp"

Eigen::Vector3d
Translate(float z, Eigen::Vector3d position, Eigen::Vector2d slope)
{
    const auto dz = z - position.z();
    return {position.x() + slope.x() * dz, position.y() + slope.y() * dz, z};
}

/** @brief
@details Adapted from the (MIT licensed) project
[corryvreckan](https://gitlab.cern.ch/corryvreckan/corryvreckan/-/blob/master/src/objects/StraightLineTrack.cpp#L89)*/
std::pair<Eigen::Vector3d, Eigen::Vector2d>
StraightLineFit(std::vector<Eigen::Vector3d> const& points, double error)
{
    Eigen::Matrix4d mat(Eigen::Matrix4d::Zero());
    Eigen::Vector4d vec(Eigen::Vector4d::Zero());
    const double errorSquared = error * error;

    for(auto& p : points)
    {
        vec += Eigen::Vector4d((p.x() / errorSquared),
                               (p.x() * p.z() / errorSquared),
                               (p.y() / errorSquared),
                               (p.y() * p.z() / errorSquared));
        Eigen::Vector4d pos(p.x(), p.x(), p.y(), p.y());
        Eigen::Matrix2d err;
        err << 1, p.z(), p.z(), p.z() * p.z();
        mat.topLeftCorner(2, 2) += err / errorSquared;
        mat.bottomRightCorner(2, 2) += err / errorSquared;
    }

    if(std::abs(mat.determinant()) < std::numeric_limits<double>::epsilon())
    {
        throw std::runtime_error(
                "Matrix inversion in straight line fit failed");
    }

    Eigen::Vector4d res = mat.inverse() * vec;
    return {{res(0), res(2), 0.}, {res(1), res(3)}};
}

/** @brief
@details Adapted from the (MIT licensed) project
[corryvreckan](https://gitlab.cern.ch/corryvreckan/corryvreckan/-/blob/master/src/objects/GblTrack.cpp#L38)*/
void GblFit(std::vector<Eigen::Vector3d> const& points,
            double error,
            double momentum)
{
}

BoundaryConditions::BoundaryConditions(const Event* const event, double sourceZ)
    : mEvent(event), mSourceZ(sourceZ)
{
}

StraightLineBoundaryConditions::StraightLineBoundaryConditions(
        const Event* const event, double sourceZ)
    : BoundaryConditions(event, sourceZ)
{
}

Eigen::Vector3d StraightLineBoundaryConditions::InPosition(double error) const
{
    if(mEvent->mDetectorPositionsFront.size() > 1)
    {
        const auto fit =
                StraightLineFit(mEvent->mDetectorPositionsFront, error);
        return Translate(
                mEvent->mPhantomPositions.front().z(), fit.first, fit.second);
    }
    else if(mEvent->mDetectorPositionsFront.size() == 1)
    {
        // special case for one plane in front (using the beam origin as point)
        auto positions = mEvent->mDetectorPositionsFront;
        positions.insert(positions.begin(),
                         {0, 0, positions.front().z() - mSourceZ});
        const auto fit = StraightLineFit(positions, error);
        return Translate(
                mEvent->mPhantomPositions.front().z(), fit.first, fit.second);
    }
    return {0, 0, 0};
}

Eigen::Vector3d StraightLineBoundaryConditions::OutPosition(double error) const
{
    const auto fit = StraightLineFit(mEvent->mDetectorPositionsBack, error);
    return Translate(
            mEvent->mPhantomPositions.back().z(), fit.first, fit.second);
}

Eigen::Vector2d StraightLineBoundaryConditions::InSlope(double error) const
{
    if(mEvent->mDetectorPositionsFront.size() > 1)
    {
        const auto fit =
                StraightLineFit(mEvent->mDetectorPositionsFront, error);
        return fit.second;
    }
    else if(mEvent->mDetectorPositionsFront.size() == 1)
    {
        // special case for one plane in front (using the beam origin as point)
        auto positions = mEvent->mDetectorPositionsFront;
        positions.insert(positions.begin(),
                         {0, 0, positions.front().z() - mSourceZ});
        const auto fit = StraightLineFit(positions, error);
        return fit.second;
    }
    return {0, 0};
}

Eigen::Vector2d StraightLineBoundaryConditions::OutSlope(double error) const
{
    const auto fit = StraightLineFit(mEvent->mDetectorPositionsBack, error);
    return fit.second;
}

GeneralBrokenLinesBoundaryConditions::GeneralBrokenLinesBoundaryConditions(
        const Event* const event, double materialBudget, double sourceZ)
    : BoundaryConditions(event, sourceZ), mMaterialBudget(materialBudget)
{
}

Eigen::Vector3d
GeneralBrokenLinesBoundaryConditions::InPosition(double error) const
{
}

Eigen::Vector3d
GeneralBrokenLinesBoundaryConditions::OutPosition(double error) const
{
}

Eigen::Vector2d
GeneralBrokenLinesBoundaryConditions::InSlope(double error) const
{
}

Eigen::Vector2d
GeneralBrokenLinesBoundaryConditions::OutSlope(double error) const
{
}
