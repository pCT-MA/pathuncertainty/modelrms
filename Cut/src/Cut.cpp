#include <numeric>

#include <TAxis.h>
#include <TCanvas.h>
#include <TEllipse.h>
#include <TFile.h>
#include <TGraph.h>
#include <TMultiGraph.h>
#include <TTree.h>

#include "BoundaryConditions.hpp"
#include "Cut.hpp"
#include "DetectorErrors.hpp"
#include "Geant4EventLoader.hpp"

std::pair<double, double> MeanStdOf(std::vector<double> const& data)
{
    double mean = std::accumulate(data.begin(), data.end(), 0.0) / data.size();
    double std = std::sqrt(
            std::inner_product(data.begin(), data.end(), data.begin(), 0.0) /
                    data.size() -
            mean * mean);
    return {mean, std};
}

Geant4EnergyLossSlopeDistribution::Geant4EnergyLossSlopeDistribution(
        std::string const& inFileName,
        std::vector<double> const& spatialResolutions)
    : mSlopes(spatialResolutions.size() > 0 ? spatialResolutions.size() : 1),
      mSpatialResolutions(spatialResolutions.size() > 0
                                  ? spatialResolutions
                                  : std::vector<double>{0})
{
    Geant4EventLoader loader(inFileName);
    std::vector<DetectorErrors> errors;
    for(auto sigmaP : mSpatialResolutions)
    {
        errors.push_back(DetectorErrors(sigmaP));
    }
    Event event;
    Event errorEvent;
    StraightLineBoundaryConditions boundaries(
            &errorEvent, loader.GetParameters().mSourceDistance);
    const auto numberOfEvents = loader.NumberOfEvents();

    mEnergyLosses.resize(numberOfEvents);
    for(auto& slopes : mSlopes)
    {
        slopes.resize(2 * numberOfEvents);
    }
    for(std::size_t entry = 0; entry < numberOfEvents; entry++)
    {
        loader.LoadEvent(event);

        mEnergyLosses[entry] =
                event.mDetectorEnergyBack - event.mDetectorEnergyFront;
        for(std::size_t r = 0; r < SpatialResolutions().size(); r++)
        {
            const auto resolution = SpatialResolutions().at(r);
            errorEvent = event;

            errors.at(resolution)(errorEvent);

            const auto slopeChange = boundaries.OutSlope(resolution) -
                                     boundaries.InSlope(resolution);

            mSlopes[r][entry] = slopeChange.x();
            mSlopes[r][numberOfEvents + entry] = slopeChange.y();
        }
    }
}

std::vector<double> const&
Geant4EnergyLossSlopeDistribution::EnergyLosses() const
{
    return mEnergyLosses;
}

std::vector<double>
Geant4EnergyLossSlopeDistribution::Slopes(double spatialResolution) const
{
    std::size_t index;
    if(IndexOf(spatialResolution, index))
    {
        return mSlopes.at(index);
    }
    return std::vector<double>();
}

std::vector<double> const&
Geant4EnergyLossSlopeDistribution::SpatialResolutions() const
{
    return mSpatialResolutions;
}

bool Geant4EnergyLossSlopeDistribution::IndexOf(double spatialResolution,
                                                std::size_t& outIndex) const
{
    for(std::size_t i = 0; i < mSpatialResolutions.size(); i++)
    {
        if(mSpatialResolutions.at(i) == spatialResolution)
        {
            outIndex = i;
            return true;
        }
    }
    return false;
}

RectangularEventCutGeant4::RectangularEventCutGeant4(
        std::string const& inFileName,
        std::string const& outFileName,
        std::vector<double> const& spatialResolutions)
    : Geant4EnergyLossSlopeDistribution(inFileName, spatialResolutions),
      mEnergyLossCut(nullptr),
      mScatterAngleCuts(SpatialResolutions().size(), nullptr)
{
    // local copy, not a ref
    auto energyLosses = EnergyLosses();
    {
        double mu, sigma;
        std::tie(mu, sigma) = MeanStdOf(EnergyLosses());
        if(mEnergyLossCut != nullptr)
        {
            delete mEnergyLossCut;
        }
        mEnergyLossCut = new NSigmaCut<double>(mu, sigma, 3.);
    }

    std::array<double, 5> rectEnergy;
    std::tie(rectEnergy[0], rectEnergy[1]) = mEnergyLossCut->GetInterval();
    rectEnergy[2] = rectEnergy.at(1);
    rectEnergy[3] = rectEnergy.at(0);
    rectEnergy[4] = rectEnergy.at(0);

    for(std::size_t r = 0; r < SpatialResolutions().size(); r++)
    {
        const auto& slopes = Slopes(SpatialResolutions().at(r));
        {
            double mu, sigma;
            std::tie(mu, sigma) = MeanStdOf(slopes);
            if(mScatterAngleCuts[r] != nullptr)
            {
                delete mScatterAngleCuts[r];
            }
            mScatterAngleCuts[r] = new NSigmaCut<double>(mu, sigma, 3.);
        }
    }

    if(outFileName.size() > 0)
    {
        std::cout << "doing the thing";
        // duplicate for drawing (x and y are merged in slope)
        energyLosses.insert(
                energyLosses.end(), energyLosses.begin(), energyLosses.end());
        TFile outFile(outFileName.c_str(), "RECREATE");

        for(std::size_t r = 0; r < SpatialResolutions().size(); r++)
        {
            const auto& slopes = Slopes(SpatialResolutions().at(r));
            TMultiGraph mg;
            auto graph = new TGraph(
                    energyLosses.size(), energyLosses.data(), slopes.data());
            mg.Add(graph, ("p" + std::to_string(r)).c_str());

            std::array<double, 5> rectAngle;
            std::tie(rectAngle[1], rectAngle[2]) =
                    mScatterAngleCuts[r]->GetInterval();
            rectAngle[0] = rectAngle.at(1);
            rectAngle[3] = rectAngle.at(2);
            rectAngle[4] = rectAngle.at(1);

            auto rectGraph = new TGraph(5, rectEnergy.data(), rectAngle.data());
            rectGraph->SetLineColor(2);
            mg.Add(rectGraph);
            mg.GetXaxis()->SetTitle("Energy loss [MeV]");
            mg.GetYaxis()->SetTitle("Direction slope");

            outFile.WriteTObject(&mg, "DataCuts");
        }
        outFile.Close();
    }
}

RectangularEventCutGeant4::~RectangularEventCutGeant4()
{
    if(mEnergyLossCut != nullptr)
    {
        delete mEnergyLossCut;
        mEnergyLossCut = nullptr;
    }
    for(auto cut : mScatterAngleCuts)
    {
        delete cut;
    }
    mScatterAngleCuts.clear();
}

bool RectangularEventCutGeant4::operator()(BoundaryConditions const& boundaries,
                                           double spatialResolution) const
{
    const auto inSlope = boundaries.InSlope(spatialResolution);
    const auto outSlope = boundaries.OutSlope(spatialResolution);
    double energyLoss = boundaries.mEvent->mDetectorEnergyBack -
                        boundaries.mEvent->mDetectorEnergyFront;
    double diffSlopeX = outSlope.x() - inSlope.x();
    double diffSlopeY = outSlope.y() - inSlope.y();

    std::size_t index;
    if(IndexOf(spatialResolution, index))
    {
        return (*mEnergyLossCut)(energyLoss) ||
               (*mScatterAngleCuts.at(index))(diffSlopeX) ||
               (*mScatterAngleCuts.at(index))(diffSlopeY);
    }
    return false;
}

CircularEventCutGeant4::CircularEventCutGeant4(
        std::string const& inFileName,
        std::string const& outFileName,
        std::vector<double> const& spatialResolutions)
    : Geant4EnergyLossSlopeDistribution(inFileName, spatialResolutions)
{
    auto energyLosses = EnergyLosses();
    std::tie(mEnergyLossCenter, mEnergyLossRadius) = MeanStdOf(EnergyLosses());
    for(std::size_t r = 0; r < SpatialResolutions().size(); r++)
    {
        const auto& slopes = Slopes(SpatialResolutions().at(r));
        std::tie(mScatteringCenter[r], mScatteringRadius[r]) =
                MeanStdOf(slopes);
        mScatteringRadius[r] *= 3.;
    }

    if(outFileName.size() > 0)
    {
        // Duplicate for drawing
        energyLosses.insert(
                energyLosses.end(), energyLosses.begin(), energyLosses.end());
        TFile outFile(outFileName.c_str(), "RECREATE");
        for(std::size_t r = 0; r < SpatialResolutions().size(); r++)
        {
            const auto& slopes = Slopes(SpatialResolutions().at(r));

            auto graph = new TGraph(
                    energyLosses.size(), energyLosses.data(), slopes.data());

            auto ellipse = new TEllipse(mEnergyLossCenter,
                                        mScatteringCenter.at(r),
                                        mEnergyLossRadius,
                                        mScatteringRadius.at(r));
            ellipse->SetLineColor(2);
            ellipse->SetFillColorAlpha(0, 1);
            graph->GetXaxis()->SetTitle("Energy loss [MeV]");
            graph->GetYaxis()->SetTitle("Direction slope");

            outFile.WriteTObject(graph, "Data");
            outFile.WriteTObject(ellipse, "DataCuts_ell");
        }
        outFile.Close();
    }
}

bool CircularEventCutGeant4::operator()(BoundaryConditions const& boundaries,
                                        double spatialResolution) const
{
    const auto inSlope = boundaries.InSlope(spatialResolution);
    const auto outSlope = boundaries.OutSlope(spatialResolution);
    double energyLoss = boundaries.mEvent->mDetectorEnergyBack -
                        boundaries.mEvent->mDetectorEnergyFront;
    double diffSlopeX = outSlope.x() - inSlope.x();
    double diffSlopeY = outSlope.y() - inSlope.y();

    std::size_t index;
    if(IndexOf(spatialResolution, index))
    {
        return !PointInEllipse(energyLoss,
                               diffSlopeX,
                               mEnergyLossCenter,
                               mScatteringCenter.at(index),
                               mEnergyLossRadius,
                               mScatteringRadius.at(index)) ||
               !PointInEllipse(energyLoss,
                               diffSlopeY,
                               mEnergyLossCenter,
                               mScatteringCenter.at(index),
                               mEnergyLossRadius,
                               mScatteringRadius.at(index));
    }
    return false;
}

bool CircularEventCutGeant4::PointInEllipse(
        double x, double y, double mx, double my, double rx, double ry)
{
    const auto dx = x - mx;
    const auto dy = y - my;
    return (dx * dx) / (rx * rx) + (dy * dy) / (ry * ry) <= 1;
}
