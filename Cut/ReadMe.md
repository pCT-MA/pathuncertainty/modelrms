# Data cuts

![DataCuts.png](doc/DataCuts.png)

In order to improve the quality of all track models, outliers such as large
angle scatter events or nuclear events are attempted to be removed by applying
data cuts. As shown in the image above, these are simply $`3\sigma`$ cuts on the 
distribution of scatter angles and the distribution of energy losses (the red
rectangle is the interval containing accepted events).

The implementation of data cuts is realised by the `EventCut` class, which reads
through the energy losses and scatter angles of the simulation data once upon
construction, to find the mean and standard deviation of both, and to calculate
the cutoff indicated as red rectangle in the figure above. 

For each event, the data cut is then asked whether to skip this event. Points
that lie outside of the rectangle are refused, while points inside are free to
be analyzed. 
