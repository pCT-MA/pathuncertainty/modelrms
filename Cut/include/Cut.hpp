#pragma once

#include <random>
#include <tuple>

#include "BoundaryConditions.hpp"

class TFile;

template <typename T> class Cut
{
public:
    /** @brief Returns true if value is supposed to be cut out. */
    virtual bool operator()(T const& value) const = 0;
};

template <typename T> class NullCut : public Cut<T>
{
public:
    virtual bool operator()(T const& value) const override { return false; };
};

template <typename T> class IntervalCut : public Cut<T>
{
public:
    IntervalCut(std::pair<T, T> const& interval) { SetInterval(interval); }

    virtual bool operator()(T const& value) const override
    {
        return !(mInterval.first < value && value < mInterval.second);
    }

    void SetInterval(std::pair<T, T> const& interval) { mInterval = interval; }
    std::pair<T, T> const& GetInterval() const { return mInterval; }

private:
    std::pair<T, T> mInterval;
};

template <typename T> class NSigmaCut : public IntervalCut<T>
{
public:
    NSigmaCut(T mean, T std, double n = 1.)
        : IntervalCut<T>({mean - n * std, mean + n * std})
    { /**/
    }
};

class Geant4EnergyLossSlopeDistribution
{
public:
    Geant4EnergyLossSlopeDistribution(
            std::string const& inFileName,
            std::vector<double> const& spatialResolutions = {});

    std::vector<double> const& EnergyLosses() const;
    std::vector<double> Slopes(double spatialResolution) const;

protected:
    std::vector<double> const& SpatialResolutions() const;

    bool IndexOf(double spatialResolution, std::size_t& outIndex) const;

private:
    static constexpr const char* gDetectorXTreeName = "detectorX";
    static constexpr const char* gDetectorYTreeName = "detectorY";
    static constexpr const char* gDetectorZTreeName = "detectorZ";
    static constexpr const char* gDetectorEnergyTreeName = "detectorE";

    std::vector<double> mEnergyLosses;

    std::vector<std::vector<double>> mSlopes;
    std::vector<double> mSpatialResolutions;
    std::size_t mSelectedSpatialResolution;
    std::default_random_engine mRNG;
};

class EventCutSpatialResolution
{
public:
    virtual bool operator()(BoundaryConditions const& value,
                            double spatialResolution) const = 0;
};

class RectangularEventCutGeant4 : public EventCutSpatialResolution,
                                  public Geant4EnergyLossSlopeDistribution
{
public:
    RectangularEventCutGeant4(
            std::string const& inFileName,
            std::string const& outFileName = "",
            std::vector<double> const& spatialResolutions = {});

    ~RectangularEventCutGeant4();

    virtual bool operator()(BoundaryConditions const& value,
                            double spatialResolution) const override;

private:
    NSigmaCut<double>* mEnergyLossCut;
    std::vector<NSigmaCut<double>*> mScatterAngleCuts;
};

class CircularEventCutGeant4 : public EventCutSpatialResolution,
                               public Geant4EnergyLossSlopeDistribution
{
public:
    CircularEventCutGeant4(std::string const& inFileName,
                           std::string const& outFileName = "",
                           std::vector<double> const& spatialResolutions = {});

    virtual bool operator()(BoundaryConditions const& value,
                            double spatialResolution) const override;

    double mEnergyLossRadius;
    double mEnergyLossCenter;
    std::vector<double> mScatteringRadius;
    std::vector<double> mScatteringCenter;

    static bool PointInEllipse(
            double x, double y, double mx, double my, double rx, double ry);
};

class NullCutSpatialResolution : public EventCutSpatialResolution
{
public:
    virtual bool operator()(BoundaryConditions const& value,
                            double spatialResolution) const override
    {
        return false;
    };
};