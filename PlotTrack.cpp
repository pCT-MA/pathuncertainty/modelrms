#include <iostream>

#include <TFile.h>
#include <TTree.h>

#include "BoundaryConditions.hpp"
#include "Cut.hpp"
#include "DetectorErrors.hpp"
#include "Geant4EventLoader.hpp"
#include "LinearTrackModel.hpp"
#include "MostLikelyPathTrackModel.hpp"
#include "SimpleSplineTrackModel.hpp"
#include "SingleKinkLinearFallbackTrackModel.hpp"

void PrintMessage(std::string const& message);

int main(int argc, char const* argv[])
{
    if(argc < 2)
    {
        PrintMessage("Use: ./PlotTrack Path/To/Data.root [sigmaP]");
        std::exit(EXIT_FAILURE);
    }

    const std::string inFileName(argv[1]);
    const auto outFileName =
            inFileName.substr(0, inFileName.find_last_of(".")) + "_tracks.root";

    double sigmaP = 0;
    for(int i = 2; i < argc; i++)
    {
        std::size_t number;
        std::stringstream parser(argv[i]);
        parser >> sigmaP;
    }

    Geant4EventLoader loader(inFileName);
    DetectorErrors errors(sigmaP);
    Event event;
    StraightLineBoundaryConditions boundaries(
            &event, loader.GetParameters().mSourceDistance);
    RectangularEventCutGeant4 eventCut(inFileName, outFileName, {sigmaP});

    std::vector<ITrackModel*> trackModels;
    trackModels.emplace_back(new LinearTrackModel(&boundaries));
    trackModels.emplace_back(
            new SingleKinkLinearFallbackTrackModel(&boundaries));
    trackModels.emplace_back(new SimpleSplineTrackModel(&boundaries));
    trackModels.emplace_back(new MostLikelyPathTrackModel(
            &boundaries, new Geant4KinematicTerm(inFileName)));

    TFile outFile(outFileName.c_str(), "RECREATE");
    TTree outTree("tracks", "tracks");
    int eventNumber = 0;
    double x;
    double y;
    double z;
    std::string model;
    outTree.Branch("event", &eventNumber);
    outTree.Branch("x", &x);
    outTree.Branch("y", &y);
    outTree.Branch("z", &z);
    outTree.Branch("sigmaP", &sigmaP);
    outTree.Branch("model", "std::string", &model);

    while(loader.LoadEvent(event))
    {
        if(eventCut(boundaries, sigmaP))
        {
            continue;
        }

        errors(event);

        for(std::size_t iZ = 0; iZ < event.mPhantomPositions.size(); iZ++)
        {
            x = event.mPhantomPositions.at(iZ).x();
            y = event.mPhantomPositions.at(iZ).y();
            z = event.mPhantomPositions.at(iZ).z();
            model = "Ground truth";
            outTree.Fill();
        }

        for(std::size_t iModel = 0; iModel < trackModels.size(); iModel++)
        {
            if(trackModels.at(iModel)->UseEvent(sigmaP))
            {
                model = trackModels.at(iModel)->GetTitle();
                // first element
                {
                    const auto inPosition = boundaries.InPosition(sigmaP);
                    x = inPosition.x();
                    y = inPosition.y();
                    z = inPosition.z();
                    outTree.Fill();
                }

                // elements [2, n-1]
                for(std::size_t iZ = 1; iZ < Event::gNumberOfLayers - 1; iZ++)
                {
                    const auto xy = trackModels.at(iModel)->ProjectXY(
                            event.mPhantomPositions.at(iZ).z(), sigmaP);
                    x = xy.x();
                    y = xy.y();
                    z = event.mPhantomPositions.at(iZ).z();
                    outTree.Fill();
                }
                // last element
                {
                    const auto outPosition = boundaries.OutPosition(sigmaP);
                    x = outPosition.x();
                    y = outPosition.y();
                    z = outPosition.z();
                    outTree.Fill();
                }
            }
        }
        ++eventNumber;
    }
    outFile.WriteTObject(&outTree);

    for(auto model : trackModels)
    {
        delete model;
    }
}

void PrintMessage(std::string const& message)
{
    std::cout << message << std::endl;
}
